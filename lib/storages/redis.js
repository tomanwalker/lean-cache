
// ## dependencies
var Redis = require("ioredis");
var log = require('debug')('lean-cache');

// ## export
module.exports = function(size, ttl, interval){
	
	// <interval> unused, Node-cache only limits time
	// <interval> present for compatibility with other storage classes
	
	log('redis.init - start - size = %s | ttl = %s', size, ttl );
	
	// ### props
	var _this = this;
	this.instance = null;
	this.count = 0;
	this.expiery = true; // the storage type has own Expiery mechanism
	this.client = true; // the storage type requires Client init
	
	// ### config
	
	
	
	// ### methods
	this.init = function(conn){
		log('redis.init - start - conn = %j', conn);
		this.instance = new Redis(conn);
		
		// TODO events - on expiery
		
		return Promise.resolve(true);
	};
	
	this.add = function(key, val){
		log('redis.add - start - key = %s | ttl = %s', key, ttl);
		// EX - seconds
		_this.instance.set(key, val, "EX", ttl).then(async function(setResult){
			_this.count = await _this.instance.command("COUNT");
		});
	};
	this.get = function(key){
		log('redis.get - start - key = %s', key);
		return _this.instance.get( key );
	};
	this.keys = async function(){
		return _this.instance.keys();
	};
	this.tail = function(){
		var k = _this.keys();
		return _this.instance.get( k[k.length - 1] ).value;
	};
	this.head = function(){
		return _this.instance.get( _this.keys()[0] ).value;
	};
	
};


