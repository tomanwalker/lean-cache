
// ## dependencies
var expect = require('chai').expect;
var path = require('path');
var proxyquire = require('proxyquire');

// config
var moduleName = path.basename(__filename);

// funcs
var print = function(...msg){
	msg[0] = moduleName + ' [TESTRUN] >> ' + msg[0];
	return console.log(...msg);
};

// target
var dirLevelUp = '../';
var unit = require(dirLevelUp + 'index.js');

// flow
describe('events', function(){
	
	it('all', function(done){
		
		this.timeout(3500);
		
		var result = {
			e: 0,
			s: 0,
			g: 0
		};
		var cache = new unit({
			size: 5,
			ttl: 2,
			interval: 1
		});
		
		cache.on('expiry', function(obj){
			print('ev.expiry - obj = %j', obj);
			result.e += 1;
		});
		cache.on('set', function(obj){
			print('ev.set - obj = %j', obj);
			result.s += 1;
		});
		cache.on('get', function(obj){
			print('ev.get - obj = %j', obj);
			result.g += 1;
		});
		
		cache.set('1', 'abc');
		
		cache.get('1', function(err, body){
			print('get 1 - err = %s | body = %j', err, body);
		});
		
		setTimeout(function(){
			
			print('ev.all - result = %j', result);
			expect( result.e > 0 ).to.be.true;
			done();
			
		}, 3000);
		
		
	});
	
});


