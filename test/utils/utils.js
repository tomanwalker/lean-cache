
// ## dependencies


// ## export
var ns = {};
module.exports = ns;

// ## config
var storage = {};
for(var i=0; i<100; i++){
	storage[i.toString()] = { name:("dummy" + i) };
};

ns.db = storage;

// ## func
ns.getLogger = function(name){
	return function(...msg){
		msg[0] = name + ' [TESTRUN] >> ' + msg[0];
		return console.log(...msg);
	};
};

ns.loadFunc = function(id, cb){
	
	if( storage[id] ){
		var val = storage[id];
		if( cb ){
			return cb(null, val);
		}
		return Promise.resolve(val);
	}
	
	var err = {
		message: 'not found - ' + id
	};
	if( cb ){
		return cb(err, null);
	}
	return Promise.reject(err);
	
};


